<?php
ob_start();
if (isset($_POST['submit']) && isset($_FILES['file'])) {
    $file = uploadFile([$_FILES['file']]);
    if ($file) {
        $file = decryption($file);
        downloadFile($file);
    }
    // die('there is a problem');
}

function uploadFile(array $input)
{
    $dir = dirname(__DIR__) . '/locked/';
    // var_dump($dir);
    foreach ($input as $item) {
        $tmpFile = $item['tmp_name'];
        $newFile = $dir . time() . '-' .  $item['name'];
        $up = move_uploaded_file($tmpFile, $newFile);
        if ($up) {
            return $newFile;
        }
        return false;
    }
}

function decryption($file)
{
    $fileName = explode('-',basename($file));
    $fileName = $fileName[1];
    $fileName =  mb_substr($fileName,0,4);
    $newFileName = dirname(__DIR__) . '/unLocked/' . mb_substr(md5(sha1(rand(121,12313))),0,7) . '.txt';
    $file = str_split(file_get_contents($file),3);
    openAndWrite(
        ['fileName'=>$fileName,'newFileName'=>$newFileName,'file'=>$file]
    );
    return $newFileName;
//    die();
}

function openAndWrite(array $arr){
    $fh = fopen($arr['newFileName'],'a+');
    foreach ($arr['file'] as $item){
        $item = (int) $item - base64_decode($arr['fileName']);
        $item = mb_chr($item,'utf8');
        fwrite($fh,$item);
    }
    fclose($fh);
}

function downloadFile($file)
{
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
}
