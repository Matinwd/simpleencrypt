<?php
/**
 * Created by PhpStorm.
 * User: Matinwd
 * Date: 11/2/2019
 * Time: 2:30 PM
 */
function downloadFile($file){
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
}
if(isset($_POST['submit'])){
    $file = uploadFile([$_FILES['file']]);
    if($file){
        $file = encryption($file);
        downloadFile($file);
    }
    // die('there is a problem');
}
function isSend(array $input) :bool
{
    foreach($input as $item){
        if(!isset($item)){
            return false;
        }
    }
    return true;

}
function uploadFile(array $input){
    $dir = dirname(__DIR__) . '/mainFiles/';
    foreach($input as $item){
        $tmpFile = $item['tmp_name'];
        $newFile = $dir . time() .  $item['name'];
        $up = move_uploaded_file($tmpFile,$newFile);
        if($up){
            return $newFile;
        }
        return false;
    }
}


function encryption($file) :string
{
    $number =rand(123,321) + 134;
    $dirName = dirname(__DIR__) . '/locked/' .base64_encode($number) . 1 . '()' . '.txt';
    $fh = fopen($dirName,'a+');
    $file = str_split(file_get_contents($file));
    foreach ($file as $item){
        $item = mb_ord($item,'utf8');
        $item = $item + $number;
        fwrite($fh,$item);
    }
    fclose($fh);
    return $dirName;
}